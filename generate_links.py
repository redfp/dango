from json import loads


result = ""
with open("./links.json") as f:
    slides = loads(f.read())
for slide in slides:
    result += '\n<div class="slide">'
    for link in slide["content"]:
        if "icon" in link.keys():
            result += f"""
  <a target="blank" href="{link["link"]}" class="card">
    <i class="card_icon" data-feather="{link["icon"]}"></i>
  </a>"""
        elif "text" in link.keys():
            result += f"""
  <a target="blank" href="{link["link"]}" class="card card_text">
    {link["text"]}
  </a>"""
    result += "\n</div>"

print(result)
