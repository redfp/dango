# Dango

Minimal startpage, inspired by Bento.
favicon.svg is taken from [FxEmojis](mozilla.github.io/fxemoji/) / [CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/)
