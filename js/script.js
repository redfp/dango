var config = {
  show_seconds: false,
  am_pm: false,
  show_year: false,
  month_first: true,
  search_engine: "https://www.google.com/search?q=%s",
};

function showTime() {
  var months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];

  var d = new Date();
  var h = d.getHours();
  var m = d.getMinutes();
  var s = d.getSeconds();
  var M = d.getMonth();
  var D = d.getDay();
  var Y = d.getFullYear();

  var time = (h < 10 ? "0" + h : h) + ":" + (m < 10 ? "0" + m : m);

  if (config.show_seconds) {
    time += ":" + (s < 10 ? "0" + s : s);
  }

  if (config.am_pm) {
    var session = "AM";
    if (h == 0) {
      h = 12;
    }

    if (h > 12) {
      h = h - 12;
      session = "PM";
    }
    time += " " + session;
  }

  document.getElementsByClassName("time").item(0).textContent = time;

  if (config.month_first) {
    var date = months[M] + " " + (D + 1).toString();
  } else {
    var date = (D + 1).toString() + " " + months[M];
  }
  if (config.show_year) {
    date += ", " + Y;
  }

  document.getElementsByClassName("date").item(0).textContent = date;

  setTimeout(showTime, 1000);
}

currentSlide = 0;

// function nextSlide() {
//   currentSlide += 1;
//   showSlides();
// }

// function previousSlide() {
//   currentSlide -= 1;
//   showSlides();
// }

// function showSlides() {
//   var i;
//   var slides = document.getElementsByClassName("slide");
//   if (currentSlide > slides.length - 1) {
//     currentSlide = 0;
//   }
//   if (currentSlide < 0) {
//     currentSlide = slides.length - 1;
//   }
//   for (i = 0; i < slides.length; i++) {
//     if (slides[i].classList.contains("slide_active")) {
//       slides[i].classList.remove("slide_active");
//     }
//   }
//   slides[currentSlide].classList.add("slide_active");
// }

function search(event) {
  const q = document.getElementsByClassName("search_bar")[0];
  event.preventDefault();
  const url = encodeURI(config.search_engine.replace("%s", q.value));
  const win = window.open(url, "_blank");
  win.focus();
}

showTime();
feather.replace();
new Splide(".splide", {
  direction: "ttb",
  height: "12rem",
  width: "200%",
}).mount();

document.getElementById("search").addEventListener("submit", search);
